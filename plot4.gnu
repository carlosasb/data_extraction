#Visualização de MDT com Gnuplot
# Autor: M. Galo, UNESP / Departamento de Cartografia
# 
#
#reset
#set term postscript enhanced color 
set term png enhanced
set output "figure_name.png"
set xlabel "Integration time t (Gyr)"
set ylabel "nomey"
fator=0
set xrange[0:3]
set yrange[]
set size 1
plot 'file_type_max_xy.dat' u ($1*0.09786):($2*fator) w line notitle
