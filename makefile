include make.config
#----------------------------------------------------------------------#
# C++ compiler

ifeq ($(CPPCOMPILER), GNU)
CPPC     = g++
endif # COMPILER

#----------------------------------------------------------------------#
# Flags

ifeq ($(COOL), Y)
COOLDIR  = cooling
else
COOLDIR  = no_cooling
endif # COOL

ifeq ($(HOST), gina)
SIMDIR     = $(HOME)/alphacrucis/$(SIMS)/$(COOLDIR)
ifeq ($(NONLOCAL), Y)
TDIR       = $(HOME)/$(OUTPUTS)
endif
endif # HOST

ifeq ($(HOST), cygwin)
SIMDIR     = /cygdrive/o/alphacrucis/$(SIMS)/$(COOLDIR)
ifeq ($(NONLOCAL), Y)
TDIR       = $(HOME)/$(OUTPUTS)
endif
endif # HOST

ifeq ($(HOST), hermes)
SIMDIR     = /cygdrive/n/alphacrucis/$(SIMS)/$(COOLDIR)
ifeq ($(NONLOCAL), Y)
TDIR       = $(HOME)/$(OUTPUTS)
endif
endif # HOST

ifeq ($(HOST), kino_gina)
SIMDIR     = $(HOME)/gina/alphacrucis/$(SIMS)/$(COOLDIR)
ifeq ($(NONLOCAL), Y)
TDIR       = $(HOME)/$(OUTPUTS)
endif
endif # HOST

#----------------------------------------------------------------------#
.SUFFIXES: .cpp .o .sh

export FORCOMPILER NAMEFOR HDF5DIR

sources = lineparser.cpp
objects = lineparser.o
execs = lineparser.x $(NAMECPP) $(NAMEFOR)

.sh: $(execs)
	chmod +x *.sh

.cpp.o: $(sources)
	$(CPPC) -c $*.cpp

lineparser.x: $(objects)
	$(CPPC) $(objects) -o lineparser.x

SUBDIRS = $(NAMEFOR)

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

clean:
	rm -rf *.o *.x *~

clean-all:
	rm -rf *.o *.x *~
	$(MAKE) -C $(NAMEFOR) clean

run:
	./$(NAME).sh $(SIMDIR) $(TDIR)

reset:
	./$()

#----------------------------------------------------------------------#
.PHONY: clean clean-all run test
.PHONY: sundirs $(SUBDIRS)
