#!/bin/bash

if [ ! -d "$1" ]
then
    echo "##### Argument "$1" is NOT a directory! #####"
    exit 1
fi

dpname=$2
while true
do
    if [[ ! -d $dpname ]]
    then 
        echo "Target parent directory "$dpname" not found!"
        echo "Press ctrl+c to escape or give a valid path:"
        read dpname
    else
        break
    fi
done

pname=$1

figold="figname.png"
aold="dtype"
xold="x=0"
for i in {1..1..1}
do
    ang=$((i*15))
    for j in {5..5..2}
    do
        fname=$pname"ang$ang/vel$j/smass100/dens_max_xy.dat"
        if [ -e "$fname" ]
        then
           cp "$fname" ./
            echo "==================================================================================================="
            echo "Pitch angle i = $ang degrees and initial velocity V = $j km/s                               "
            echo "==================================================================================================="
            for k in {1..151..1}
            do
                fignew="densmax$k.png"
                anew="every ::$((k-1))::$((k-1))"
                sed -i "s/$figold/$fignew/g" 'plot5.gnu'
                sed -i "s/$aold/$anew/g" 'plot5.gnu'
                xnew="x=$k"
                sed -i "s/$xold/$xnew/g" 'plot5.gnu'
                gnuplot plot5.gnu
                xold=$xnew
                aold=$anew
                figold=$fignew
            done
            rm -f "dens_max_xy.dat"
            aold="dtype"
            figold="figname.png"
            xold="x=0"
            sed -i "s/$anew/dtype/g" 'plot5.gnu'
            sed -i "s/$fignew/figname.png/g" 'plot5.gnu'
            sed -i "s/$xnew/x=0/g" 'plot5.gnu'
            tfname=$dpname"ang$ang/vel$j"
            if [ ! -d "$tfname" ]
            then
               mkdir -p "$tfname"
            fi
            mv *.png "$tfname"
            echo "DONE"
            echo "==================================================================================================="
        else
            echo "No $fname datafile found! Moving on!"
            echo "==================================================================================================="
        fi
    done
done
