#Visualização de MDT com Gnuplot
# Autor: M. Galo, UNESP / Departamento de Cartografia
# 
#
#reset
#set term postscript enhanced color 
set term png
set output "velc1.png"
#set xlabel "Índice da célula"
#set ylabel "Densidade"
set xrange[1:512]
set yrange[-0.1:0.4]
set logscale cb
set size 1
a=1
scale=20
plot 'velc_slices_xy.dat' index a u 1:2 w points notitle
