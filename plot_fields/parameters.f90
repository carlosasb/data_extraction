module parameters

implicit none

integer :: snap_first = 0, snap_last = 0, cut = 1, jumpv = 1
character(len=255) :: dir_data = './', dir_out = './'
character(len=5) :: field_name = 'dens'
character(len=3) :: minmax = 'max'
character(len=2) :: plane = 'xy'
character(len=1) :: ftype = 'p'

contains

!==============================================================================
subroutine read_parameters

implicit none
character(len=255) :: file_param, line, name, value

file_param = 'parameters.in'

open(unit=1, file=file_param, status='old')

10 read(unit=1, fmt="(a)", end=20) line
  call parse_line(line, name, value)
  select case (name)
    case ('dir_data')
      read(value,*) dir_data
    case ('dir_out')
      read(value,*) dir_out
    case ('snap_first')
      read(value,*) snap_first
    case ('snap_last')
      read(value,*) snap_last
    case ('field_name')
      read(value,*) field_name
    case ('plane')
      read(value,*) plane
    case ('cut')
      read(value,*) cut
    case ('ftype')
      read(value,*) ftype
    case ('jumpv')
      read(value,*) jumpv
    case ('minmax')
      read(value,*) minmax
    case default
  end select
  go to 10

20 close(unit=1)

end subroutine read_parameters
!##############################################################################


!==============================================================================
! from grezgorz kowal:
subroutine parse_line(line, name, value)
  character(len=*), intent(in)  :: line
  character(len=*), intent(out) :: name, value
  integer :: l, i
  l = len_trim(line)
  i = index( line, '=' )
  name  = trim(adjustl(line(1:i-1)))
  value = trim(adjustl(line(i+1:l)))
end subroutine parse_line
!##############################################################################

end module parameters
