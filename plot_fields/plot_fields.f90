! author: Reinaldo Santos de Lima
! date: 10/19/2010
! 
!   Last Reinaldo's modifications:
!   11/23/2010 - inclusion of variable vjump;
!   10/19/2010 - inclusion of the fields kmhd1 (magn. moment) and kmhd2 
!                (invariant related to the parallel pressure);
! 
program plot_fields

use parameters, only : read_parameters, dir_data, dir_out, snap_first, snap_last &
                     , ftype, field_name, plane, cut, jumpv, minmax
use hdf5_io,    only : hdf5_get_size, hdf5_load_3d_field

implicit none
integer :: n1, n2, n3, m1, m2, size_matrix(3), snap, i, j, yeah, k
real, dimension(:,:,:), allocatable :: field
real, dimension(:,:)  , allocatable :: slice1, slice2
real:: val_max, mod_v, time, val_max_temp
character(len=255) :: file_out, file_out_2
logical :: info

call read_parameters

call hdf5_get_size(dir_data, ftype, snap_first, size_matrix)

n1 = size_matrix(1)
n2 = size_matrix(2)
n3 = size_matrix(3)

select case (plane)
case ('xy', 'yx')
  plane='xy'
  m1=n1; m2=n2
case ('yz', 'zy')
  plane='yz'
  m1=n2; m2=n3
case ('xz', 'zx')
  plane='xz'
  m1=n1; m2=n3
end select

allocate (field(n1,n2,n3), slice1(m1,m2), slice2(m1,m2))

write(file_out, "(a,a,'_slices_',a,'.dat')") trim(dir_out), trim(field_name), plane
open(unit=1, file=file_out, status='replace')

!print*,trim(dir_out),file_out
inquire(file = file_out, exist = info)
!print*,info
if (.not. info) then
  write (*,'(a)') 'ERROR: ' // trim(file_out) //                              &
                  ' does not exist! Please check your setup.'
  stop
end if

select case (field_name)
  case ('velv', 'velx', 'vely', 'velz', 'dens', 'velc', 'pkin')
    select case (minmax)
      case ('max')
        write(file_out_2, "(a,a,'_max_',a,'.dat')") trim(dir_out), trim(field_name), plane
        open(unit=2, file=file_out_2, status='replace')
      case ('min')
        write(file_out_2, "(a,a,'_min_',a,'.dat')") trim(dir_out), trim(field_name), plane
        open(unit=2, file=file_out_2, status='replace')
    end select
    k=1; yeah=0
end select

do snap = snap_first, snap_last

  call get_slice(snap, field_name, plane, field, slice1, slice2)
  
  time=0.2*(snap+1)
  select case (field_name)
    case ('velx', 'vely', 'velz')
      k=1
      yeah=0
      do while (yeah.eq.0)
        val_max=slice1(255,k)
        yeah=1
        select case (minmax)
          case ('max')
            do j=1,m2
              mod_v=slice1(255,j)
              if (val_max.lt.mod_v) then
                yeah=0
              endif
            end do
          case ('min')
            do j=1,m2
              mod_v=slice1(255,j)
              if (val_max.gt.mod_v) then
                yeah=0
              endif
            end do
        end select
        k=k+1
      end do
      write(2,'(2(e14.7,2x))') time, val_max
    case ('dens')
      k=1
      yeah=0
      do while (yeah.eq.0)
        val_max=slice1(255,k)
        yeah=1
        select case (minmax)
          case ('max')
            do j=1,m2
              mod_v=slice1(255,j)
              if (val_max.lt.mod_v) then
                yeah=0
              endif
            end do
          case ('min')
            do j=1,m2
              mod_v=slice1(255,j)
              if (val_max.gt.mod_v) then
                yeah=0
              endif
           end do
        end select
        k=k+1
      end do
      write(2,'(2(e14.7,2x))') time, val_max
      do i=1,m1; do j=1,m2
        write(1,'(2(i4.4,3x),e12.4)') i, j, slice1(i,j)
      end do; end do
    case ('velv', 'magv')
      k=1
      yeah=0
      do while (yeah.eq.0)
        val_max=sqrt(slice1(255,k)**2+slice2(255,k)**2)
        yeah=1
        select case (minmax)
          case ('max')
            do j=1,m2
              mod_v=sqrt(slice1(255,j)**2+slice2(255,j)**2)
              if (val_max.lt.mod_v) then
                yeah=0
              endif
            end do
          case ('min')
            do j=1,m2
              mod_v=sqrt(slice1(255,j)**2+slice2(255,j)**2)
              if (val_max.gt.mod_v) then
                yeah=0
              endif
            end do
        end select
        k=k+1
      end do
        write(2,'(2(e14.7,2x))') time, val_max
      do i=1,m1,jumpv; do j=1,m2,jumpv
        write(1,'(2(i4.4,3x),3(e12.4,3x))') i, j, slice1(i,j), slice2(i,j), sqrt(slice1(i,j)**2+slice2(i,j)**2)
      end do; end do
    case ('vexy')
      do i=1,m1; do j=1,m2
        write(1,'(2(i4.4,3x),2(e12.4,2x))') i, j, slice1(i,j), slice2(i,j)
      end do; end do
    case ('vell')
      do i=1,m1;
        write(1,'(i4.4,3x,e12.4)') i, slice1(i,255)
      end do
    case ('velc', 'pkin')
      k=1
      yeah=0
      do while (yeah.eq.0)
        val_max=slice1(255,k)
        yeah=1
        select case (minmax)
          case ('max')
            do j=1,m2
              mod_v=slice1(255,j)
              if (val_max.lt.mod_v) then
                yeah=0
              endif
            end do
          case ('min')
            do j=1,m2
              mod_v=slice1(255,j)
              if (val_max.gt.mod_v) then
                yeah=0
              endif
            end do
        end select
        k=k+1
      end do
      write(2,'(2(e14.7,2x))') time, val_max
      do j=1,m2;
        write(1,'(i4.4,3x,e12.4)') j, slice1(255,j)
      end do
    case ('denc')
      do j=1,m2;
        write(1,'(i4.4,3x,e12.4)') j, slice1(255,j)
      end do
    case ('velr')
      do j=1,m2
        write(1,'(i4.4,3x,e12.4)') j, slice1(255,j)
      end do
    case default
      do i=1,m1; do j=1,m2
        write(1,'(2(i4.4,3x),e12.4)') i, j, slice1(i,j)
      end do; end do
    end select
    write(1,*); write(1,*)

end do


deallocate (field, slice1, slice2)

close(unit=1)
close(unit=2)


contains

!==============================================================================
subroutine get_slice(snap, field_name, plane, field, slice1, slice2)

implicit none
integer, intent(in) :: snap
character(len=5), intent(in) :: field_name
character(len=2), intent(in) :: plane
real, dimension(:,:,:), intent(inout) :: field
real, dimension(:,:), intent(inout) :: slice1, slice2

select case (field_name)
  case ('dens', 'pres', 'velx', 'vely', 'velz', 'magx', 'magy', 'magz', 'ppar', 'pper', 'qpar', 'qper','gpsi', &
        'momx', 'momy', 'momz', 'potx', 'poty', 'potz')
    call hdf5_load_3d_field(dir_data, ftype, snap, trim(field_name), field)
    call get_slice_aux(plane, field, slice1)
  case ('denc')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'dens', field)
    call get_slice_aux(plane, field, slice1)
  case ('logd')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'dens', field)
    call get_slice_aux(plane, field, slice1)
    slice1 = alog10(slice1)
  case ('emag')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magx', field)
    call get_slice_aux(plane, field, slice1)
    slice1 = slice1**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magy', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magz', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    slice1 = 0.5*slice1
  case ('ekin')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'velx', field)
    call get_slice_aux(plane, field, slice1)
    slice1 = slice1**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'vely', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'velz', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'dens', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = 0.5*slice1*slice2
  case ('pkin')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'velx', field)
    call get_slice_aux(plane, field, slice1)
    slice1 = slice1**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'vely', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'velz', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'dens', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = 0.5*slice1*slice2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'pres', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice2/slice1
  case ('calf')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magx', field)
    call get_slice_aux(plane, field, slice1)
    slice1 = slice1**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magy', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magz', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'dens', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = sqrt(slice1/slice2)
  case ('csnd')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'pres', field)
    call get_slice_aux(plane, field, slice1)
    slice1 = slice1
    call hdf5_load_3d_field(dir_data, ftype, snap, 'dens', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = sqrt(slice1/slice2)
  case ('kmhd1')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magx', field)
    call get_slice_aux(plane, field, slice1)
    slice1 = slice1**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magy', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magz', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    slice2 = sqrt(slice1)
    call hdf5_load_3d_field(dir_data, ftype, snap, 'dens', field)
    call get_slice_aux(plane, field, slice1)
    slice2 = slice1*slice2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'pper', field)
    call get_slice_aux(plane, field, slice1)
    slice1 = slice1/slice2
  case ('kmhd2')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magx', field)
    call get_slice_aux(plane, field, slice1)
    slice1 = slice1**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magy', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'magz', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1 + slice2**2
    call hdf5_load_3d_field(dir_data, ftype, snap, 'dens', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1/(slice2**3)
    call hdf5_load_3d_field(dir_data, ftype, snap, 'ppar', field)
    call get_slice_aux(plane, field, slice2)
    slice1 = slice1*slice2
  case ('vexy')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'velx', field)
    call get_slice_aux(plane, field, slice1)
    call hdf5_load_3d_field(dir_data, ftype, snap, 'vely', field)
    call get_slice_aux(plane, field, slice2)
  case ('vell')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'velx', field)
    call get_slice_aux(plane, field, slice1)
  case ('velc')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'vely', field)
    call get_slice_aux(plane, field, slice1)
  case ('velr')
    call hdf5_load_3d_field(dir_data, ftype, snap, 'velx', field)
    call get_slice_aux(plane, field, slice1)
  case ('velv')
    select case (plane)
      case ('xy', 'yx')
        call hdf5_load_3d_field(dir_data, ftype, snap, 'velx', field)
        call get_slice_aux(plane, field, slice1)
        call hdf5_load_3d_field(dir_data, ftype, snap, 'vely', field)
        call get_slice_aux(plane, field, slice2)
      case ('yz', 'zy')
        call hdf5_load_3d_field(dir_data, ftype, snap, 'vely', field)
        call get_slice_aux(plane, field, slice1)
        call hdf5_load_3d_field(dir_data, ftype, snap, 'velz', field)
        call get_slice_aux(plane, field, slice2)
      case ('xz', 'zx')
        call hdf5_load_3d_field(dir_data, ftype, snap, 'velx', field)
        call get_slice_aux(plane, field, slice1)
        call hdf5_load_3d_field(dir_data, ftype, snap, 'velz', field)
        call get_slice_aux(plane, field, slice2)
    end select
  case ('magv')
    select case (plane)
      case ('xy', 'yx')
        call hdf5_load_3d_field(dir_data, ftype, snap, 'magx', field)
        call get_slice_aux(plane, field, slice1)
        call hdf5_load_3d_field(dir_data, ftype, snap, 'magy', field)
        call get_slice_aux(plane, field, slice2)
      case ('yz', 'zy')
        call hdf5_load_3d_field(dir_data, ftype, snap, 'magy', field)
        call get_slice_aux(plane, field, slice1)
        call hdf5_load_3d_field(dir_data, ftype, snap, 'magz', field)
        call get_slice_aux(plane, field, slice2)
      case ('xz', 'zx')
        call hdf5_load_3d_field(dir_data, ftype, snap, 'magx', field)
        call get_slice_aux(plane, field, slice1)
        call hdf5_load_3d_field(dir_data, ftype, snap, 'magz', field)
        call get_slice_aux(plane, field, slice2)
    end select
end select

end subroutine get_slice
!##############################################################################

!==============================================================================
subroutine get_slice_aux(plane, field, slice)
implicit none
character(len=2), intent(in) :: plane
real, dimension(:,:,:), intent(in) :: field
real, dimension(:,:), intent(out) :: slice
  select case (plane)
  case ('xy', 'yx')
    slice(:,:) = field(:,:,cut)
  case ('yz', 'zy')
    slice(:,:) = field(cut,:,:)
  case ('xz', 'zx')
    slice(:,:) = field(:,cut,:)
  end select
end subroutine get_slice_aux
!##############################################################################

end program plot_fields
