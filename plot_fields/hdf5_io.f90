! author: Reinaldo Santos de Lima
! date: 10/15/2010
! 
!   Last Reinaldo's modifications:
!   10/15/2010 - inclusion of subroutines hdf5_load_bext and hdf5_overwrite_bext;
! 
module hdf5_io

use hdf5

contains

!==============================================================================
subroutine hdf5_get_size(dir_data, ftype, snap, size_matrix)

implicit none

character(len=255)   , intent(in)  :: dir_data
character(len=1)     , intent(in)  :: ftype
integer              , intent(in)  :: snap
integer, dimension(3), intent(out) :: size_matrix

character(len=255)    :: filename
integer               :: err, ng
integer, dimension(3) :: dm, pdm

integer(hid_t)                 :: fid, gid, aid
integer(hsize_t), dimension(1) :: cm3 = (/3/), cm1 = (/1/)


dm (:) = 1
pdm(:) = 1


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'pdims', aid, err)
      call h5aread_f(aid, h5t_native_integer, pdm, cm3, err)
      call h5aclose_f(aid, err)

      call h5aopen_name_f(gid, 'dims', aid, err)
      call h5aread_f(aid, h5t_native_integer, dm, cm3, err)
      call h5aclose_f(aid, err)

      if(ftype.eq.'r') then
        call h5aopen_name_f(gid, 'ng', aid, err)
        call h5aread_f(aid, h5t_native_integer, ng, cm1, err)
        call h5aclose_f(aid, err)
      endif

   call h5gclose_f(gid, err)

call h5fclose_f(fid, err)

call h5close_f(err)

if(ftype.eq.'r') then
  size_matrix(:) = pdm(:)*dm(:) - (pdm(:)-1)*2*ng
else
  size_matrix(:) = pdm(:)*dm(:)
endif

end subroutine hdf5_get_size
!##############################################################################

!==============================================================================
subroutine hdf5_get_time(dir_data, ftype, snap, t)

implicit none

character(len=255)    , intent(in)  :: dir_data
character(len=1)      , intent(in)  :: ftype
integer               , intent(in)  :: snap
real                  , intent(out) :: t

character(len=255)  :: filename
integer             :: err

integer(hid_t)                 :: fid, gid, aid
integer(hsize_t), dimension(1) :: cm1 = (/1/)


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'time', aid, err)
      call h5aread_f(aid, h5t_native_real, t, cm1, err)
      call h5aclose_f(aid, err)

    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)

call h5close_f(err)

end subroutine hdf5_get_time
!##############################################################################

!==============================================================================
subroutine hdf5_load_3d_field(dir_data, ftype, snap, field_name, field)

implicit none

character(len=255)    , intent(in)    :: dir_data
character(len=1)      , intent(in)    :: ftype
integer               , intent(in)    :: snap
character(len=5)      , intent(in)    :: field_name
real, dimension(:,:,:), intent(inout) :: field

character(len=255)    :: filename
integer               :: ncpus, cpu, err, ng
integer, dimension(3) :: dm, pdm, pcoords, xi, xf

real, dimension(:,:,:), allocatable :: temp_field

integer(hid_t)                 :: fid, gid, aid, did
integer(hsize_t), dimension(1) :: cm3 = (/3/), cm1 = (/1/)


dm (:) = 1
pdm(:) = 1


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'pdims', aid, err)
      call h5aread_f(aid, h5t_native_integer, pdm, cm3, err)
      call h5aclose_f(aid, err)

      call h5aopen_name_f(gid, 'dims', aid, err)
      call h5aread_f(aid, h5t_native_integer, dm, cm3, err)
      call h5aclose_f(aid, err)

      if(ftype.eq.'r') then
        call h5aopen_name_f(gid, 'ng', aid, err)
        call h5aread_f(aid, h5t_native_integer, ng, cm1, err)
        call h5aclose_f(aid, err)
      endif

    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)


  allocate (temp_field(dm(1),dm(2),dm(3)))

  ncpus = pdm(1)*pdm(2)*pdm(3)


  do cpu=0,ncpus-1

  write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, cpu

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'variables', gid, err)

      call h5dopen_f(gid, trim(field_name), did, err)
      call h5dread_f(did, h5t_native_real, temp_field, cm3, err)
      call h5dclose_f(did, err)

    call h5gclose_f(gid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'pcoords', aid, err)
      call h5aread_f(aid, h5t_native_integer, pcoords, cm3, err)
      call h5aclose_f(aid, err)

    call h5gclose_f(gid, err)

  call h5fclose_f(fid,err)

  print*
  print*, '   ncpu: ', cpu
  print*, 'pcoords: ', pcoords
  print*, '    dim: ', dm
  print*, filename, trim(field_name), h5t_native_integer

  if(ftype.eq.'r') then
    xi(:) = pcoords(:)*dm(:) + 1 - pcoords(:)*2*ng
    xf(:) = xi(:) + dm(:) - 1
  else
    xi(:) = pcoords(:)*dm(:) + 1
    xf(:) = xi(:) + dm(:) - 1
  endif

  print*, 'xi,xf: ', xi(1), xf(1)
  print*, 'yi,yf: ', xi(2), xf(2)
  print*, 'zi,zf: ', xi(3), xf(3)

  field(xi(1):xf(1), xi(2):xf(2), xi(3):xf(3)) = temp_field

  end do


deallocate (temp_field)


call h5close_f(err)


end subroutine hdf5_load_3d_field
!##############################################################################

!==============================================================================
subroutine hdf5_overwrite_3d_field(dir_data, ftype, snap, field_name, field)

implicit none

character(len=255)    , intent(in)    :: dir_data
character(len=1)      , intent(in)    :: ftype
integer               , intent(in)    :: snap
character(len=5)      , intent(in)    :: field_name
real, dimension(:,:,:), intent(inout) :: field

character(len=255)    :: filename
integer               :: ncpus, cpu, err, ng
integer, dimension(3) :: dm, pdm, pcoords, xi, xf

real, dimension(:,:,:), allocatable :: temp_field

integer(hid_t)                 :: fid, gid, aid, did
integer(hsize_t), dimension(1) :: cm3 = (/3/), cm1 = (/1/)


dm (:) = 1
pdm(:) = 1


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'pdims', aid, err)
      call h5aread_f(aid, h5t_native_integer, pdm, cm3, err)
      call h5aclose_f(aid, err)

      call h5aopen_name_f(gid, 'dims', aid, err)
      call h5aread_f(aid, h5t_native_integer, dm, cm3, err)
      call h5aclose_f(aid, err)

      if(ftype.eq.'r') then
        call h5aopen_name_f(gid, 'ng', aid, err)
        call h5aread_f(aid, h5t_native_integer, ng, cm1, err)
        call h5aclose_f(aid, err)
      endif

    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)


  allocate (temp_field(dm(1),dm(2),dm(3)))

  ncpus = pdm(1)*pdm(2)*pdm(3)


  do cpu=0,ncpus-1

  write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, cpu

  call h5fopen_f(filename, h5f_acc_rdwr_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'pcoords', aid, err)
      call h5aread_f(aid, h5t_native_integer, pcoords, cm3, err)
      call h5aclose_f(aid, err)

    call h5gclose_f(gid, err)

    print*
    print*, '   ncpu: ', cpu
    print*, 'pcoords: ', pcoords
    print*, '    dim: ', dm

    if(ftype.eq.'r') then
      xi(:) = pcoords(:)*dm(:) + 1 - pcoords(:)*2*ng
      xf(:) = xi(:) + dm(:) - 1
    else
      xi(:) = pcoords(:)*dm(:) + 1
      xf(:) = xi(:) + dm(:) - 1
    endif

    print*, 'xi,xf: ', xi(1), xf(1)
    print*, 'yi,yf: ', xi(2), xf(2)
    print*, 'zi,zf: ', xi(3), xf(3)

    temp_field = field(xi(1):xf(1), xi(2):xf(2), xi(3):xf(3))


    call h5gopen_f(fid, 'variables', gid, err)

      call h5dopen_f(gid, trim(field_name), did, err)
      call h5dwrite_f(did, h5t_native_real, temp_field, cm3, err)
      call h5dclose_f(did, err)

    call h5gclose_f(gid, err)


  call h5fclose_f(fid,err)

  end do


deallocate (temp_field)


call h5close_f(err)


end subroutine hdf5_overwrite_3d_field
!##############################################################################

!==============================================================================
subroutine hdf5_load_coord(dir_data, ftype, snap, x, y, z)

implicit none

character(len=255), intent(in)    :: dir_data
character(len=1)  , intent(in)    :: ftype
integer           , intent(in)    :: snap
real, dimension(:), intent(inout) :: x, y, z

character(len=255)    :: filename
integer               :: ncpus, cpu, err, ng
integer, dimension(3) :: dm, pdm, pcoords, xi, xf

real, dimension(:), allocatable :: x_temp, y_temp, z_temp

integer(hid_t)                 :: fid, gid, aid, did
integer(hsize_t), dimension(1) :: cm3 = (/3/), cm1 = (/1/)


dm (:) = 1
pdm(:) = 1


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'pdims', aid, err)
      call h5aread_f(aid, h5t_native_integer, pdm, cm3, err)
      call h5aclose_f(aid, err)

      call h5aopen_name_f(gid, 'dims', aid, err)
      call h5aread_f(aid, h5t_native_integer, dm, cm3, err)
      call h5aclose_f(aid, err)

      if(ftype.eq.'r') then
        call h5aopen_name_f(gid, 'ng', aid, err)
        call h5aread_f(aid, h5t_native_integer, ng, cm1, err)
        call h5aclose_f(aid, err)
      endif

    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)


  allocate (x_temp(dm(1)), y_temp(dm(2)), z_temp(dm(3)))

  ncpus = pdm(1)*pdm(2)*pdm(3)


  do cpu=0,ncpus-1

  write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, cpu

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'coordinates', gid, err)

      call h5dopen_f(gid, 'xc', did, err)
      call h5dread_f(did, h5t_native_real, x_temp, cm1, err)
      call h5dclose_f(did, err)

      call h5dopen_f(gid, 'yc', did, err)
      call h5dread_f(did, h5t_native_real, y_temp, cm1, err)
      call h5dclose_f(did, err)

      call h5dopen_f(gid, 'zc', did, err)
      call h5dread_f(did, h5t_native_real, z_temp, cm1, err)
      call h5dclose_f(did, err)

    call h5gclose_f(gid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'pcoords', aid, err)
      call h5aread_f(aid, h5t_native_integer, pcoords, cm3, err)
      call h5aclose_f(aid, err)

    call h5gclose_f(gid, err)

  call h5fclose_f(fid,err)

  print*
  print*, '   ncpu: ', cpu
  print*, 'pcoords: ', pcoords
  print*, '    dim: ', dm

  if(ftype.eq.'r') then
    xi(:) = pcoords(:)*dm(:) + 1 - pcoords(:)*2*ng
    xf(:) = xi(:) + dm(:) - 1
  else
    xi(:) = pcoords(:)*dm(:) + 1
    xf(:) = xi(:) + dm(:) - 1
  endif

  x(xi(1):xf(1)) = x_temp
  y(xi(2):xf(2)) = y_temp
  z(xi(3):xf(3)) = z_temp

  end do


deallocate (x_temp, y_temp, z_temp)


call h5close_f(err)


end subroutine hdf5_load_coord
!##############################################################################

!==============================================================================
subroutine hdf5_load_units(dir_data, ftype, snap, udens, udist, uvelo)

implicit none

character(len=255), intent(in)  :: dir_data
character(len=1)  , intent(in)  :: ftype
integer           , intent(in)  :: snap
real              , intent(out) :: udens, udist, uvelo

character(len=255) :: filename
integer            :: err

integer(hid_t)                 :: fid, gid, aid
integer(hsize_t), dimension(1) :: cm1 = (/1/)


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'udens', aid, err)
      call h5aread_f(aid, h5t_native_real, udens, cm1, err)
      call h5aclose_f(aid, err)

      call h5aopen_name_f(gid, 'udist', aid, err)
      call h5aread_f(aid, h5t_native_real, udist, cm1, err)
      call h5aclose_f(aid, err)

      call h5aopen_name_f(gid, 'uvelo', aid, err)
      call h5aread_f(aid, h5t_native_real, uvelo, cm1, err)
      call h5aclose_f(aid, err)

    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)

call h5close_f(err)

write(*,*) 'udens=', udens
write(*,*) 'udist=', udist
write(*,*) 'uvelo=', uvelo

end subroutine hdf5_load_units
!##############################################################################

!==============================================================================
subroutine hdf5_load_nsinks(dir_data, ftype, snap, nsinks)

implicit none

character(len=255), intent(in)    :: dir_data
character(len=1)  , intent(in)    :: ftype
integer           , intent(in)    :: snap
integer           , intent(inout) :: nsinks

character(len=255)    :: filename
integer               :: err
  
integer(hid_t)                 :: fid, gid, aid, did
integer(hsize_t), dimension(1) :: cm3 = (/3/), cm1 = (/1/)


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'sink_particles', gid, err)
    
      call h5dopen_f(gid, 'nsinks', did, err)
      call h5dread_f(did, h5t_native_integer, nsinks, cm1, err)
      call h5dclose_f(did, err)
    
    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)

call h5close_f(err)


end subroutine hdf5_load_nsinks
!##############################################################################

!==============================================================================
subroutine hdf5_load_sinksdata(dir_data, ftype, snap, sp, mdot)

implicit none

character(len=255)  , intent(in)    :: dir_data
character(len=1)    , intent(in)    :: ftype
integer             , intent(in)    :: snap
real, dimension(:,:), intent(inout) :: sp
real, dimension(:)  , intent(inout) :: mdot

character(len=255)    :: filename
integer               :: err
  
integer(hid_t)                 :: fid, gid, aid, did
integer(hsize_t), dimension(1) :: cm3 = (/3/), cm1 = (/1/), am = (/1/)

integer :: nsinks
real, dimension(:), allocatable :: sptmp


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'sink_particles', gid, err)
    
      call h5dopen_f(gid, 'nsinks', did, err)
      call h5dread_f(did, h5t_native_integer, nsinks, cm1, err)
      call h5dclose_f(did, err)

      if(nsinks.gt.0) then

        am(1) = nsinks
        allocate(sptmp(nsinks))

        call h5dopen_f(gid, 'sp_posx', did, err)
        call h5dread_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)
        sp(1,1:nsinks) = sptmp(:)

        call h5dopen_f(gid, 'sp_posy', did, err)
        call h5dread_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)
        sp(2,1:nsinks) = sptmp(:)

        call h5dopen_f(gid, 'sp_posz', did, err)
        call h5dread_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)
        sp(3,1:nsinks) = sptmp(:)

        call h5dopen_f(gid, 'sp_mass', did, err)
        call h5dread_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)
        sp(4,1:nsinks) = sptmp(:)

        call h5dopen_f(gid, 'sp_velx', did, err)
        call h5dread_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)
        sp(5,1:nsinks) = sptmp(:)

        call h5dopen_f(gid, 'sp_vely', did, err)
        call h5dread_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)
        sp(6,1:nsinks) = sptmp(:)

        call h5dopen_f(gid, 'sp_velz', did, err)
        call h5dread_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)
        sp(7,1:nsinks) = sptmp(:)

        call h5dopen_f(gid, 'sp_heat', did, err)
        call h5dread_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)
        sp(8,1:nsinks) = sptmp(:)

        call h5dopen_f(gid, 'sp_dmdt', did, err)
        call h5dread_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)
        mdot(1:nsinks) = sptmp(:)

        deallocate(sptmp)

      endif

    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)
  
call h5close_f(err)


end subroutine hdf5_load_sinksdata
!##############################################################################

!==============================================================================
subroutine hdf5_overwrite_sinksdata(dir_data, ftype, snap, nsinks, sp, mdot)

implicit none

character(len=255)  , intent(in) :: dir_data
character(len=1)    , intent(in) :: ftype
integer             , intent(in) :: snap
integer             , intent(in) :: nsinks
real, dimension(:,:), intent(in) :: sp
real, dimension(:)  , intent(in) :: mdot

character(len=255)    :: filename
integer               :: ncpus, cpu, err
integer, dimension(3) :: pdm
  
integer(hid_t)                 :: fid, gid, aid, did
integer(hsize_t), dimension(1) :: cm3 = (/3/), cm1 = (/1/), am = (/1/)

real, dimension(:), allocatable :: sptmp


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)

      call h5aopen_name_f(gid, 'pdims', aid, err)
      call h5aread_f(aid, h5t_native_integer, pdm, cm3, err)
      call h5aclose_f(aid, err)

    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)


  ncpus = pdm(1)*pdm(2)*pdm(3)


  do cpu=0,ncpus-1

  write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, cpu

  call h5fopen_f(filename, h5f_acc_rdwr_f, fid, err)

    call h5gopen_f(fid, 'sink_particles', gid, err)
    
      call h5dopen_f(gid, 'nsinks', did, err)
      call h5dwrite_f(did, h5t_native_integer, nsinks, cm1, err)
      call h5dclose_f(did, err)

      if(nsinks.gt.0) then

        am(1) = nsinks
        allocate(sptmp(nsinks))

        sptmp(:) = sp(1,1:nsinks)
        call h5dopen_f(gid, 'sp_posx', did, err)
        call h5dwrite_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)

        sptmp(:) = sp(2,1:nsinks)
        call h5dopen_f(gid, 'sp_posy', did, err)
        call h5dwrite_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)

        sptmp(:) = sp(3,1:nsinks)
        call h5dopen_f(gid, 'sp_posz', did, err)
        call h5dwrite_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)

        sptmp(:) = sp(4,1:nsinks)
        call h5dopen_f(gid, 'sp_mass', did, err)
        call h5dwrite_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)

        sptmp(:) = sp(5,1:nsinks)
        call h5dopen_f(gid, 'sp_velx', did, err)
        call h5dwrite_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)

        sptmp(:) = sp(6,1:nsinks)
        call h5dopen_f(gid, 'sp_vely', did, err)
        call h5dwrite_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)

        sptmp(:) = sp(7,1:nsinks)
        call h5dopen_f(gid, 'sp_velz', did, err)
        call h5dwrite_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)

        sptmp(:) = sp(8,1:nsinks)
        call h5dopen_f(gid, 'sp_heat', did, err)
        call h5dwrite_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)

        sptmp(:) = mdot(1:nsinks)
        call h5dopen_f(gid, 'sp_dmdt', did, err)
        call h5dwrite_f(did, h5t_native_real, sptmp, am(1:1), err)
        call h5dclose_f(did, err)

        deallocate(sptmp)

      endif

    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)
  
  enddo
  
call h5close_f(err)


end subroutine hdf5_overwrite_sinksdata
!##############################################################################

!==============================================================================
subroutine hdf5_load_bext(dir_data, ftype, snap, bext)

implicit none

character(len=255), intent(in)    :: dir_data
character(len=1)  , intent(in)    :: ftype
integer           , intent(in)    :: snap
real, dimension(3), intent(inout) :: bext

character(len=255)    :: filename
integer               :: err
  
integer(hid_t)                 :: fid, gid, aid, did
integer(hsize_t), dimension(1) :: cm3 = (/3/), cm1 = (/1/)


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdonly_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)
    
      call h5aopen_name_f(gid, 'bext', aid, err)
      call h5aread_f(aid, h5t_native_real, bext, cm3, err)
      call h5aclose_f(aid, err)
    
    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)

call h5close_f(err)


end subroutine hdf5_load_bext
!##############################################################################

!==============================================================================
subroutine hdf5_overwrite_bext(dir_data, ftype, snap, bext)

implicit none

character(len=255), intent(in) :: dir_data
character(len=1)  , intent(in) :: ftype
integer           , intent(in) :: snap
real, dimension(3), intent(in) :: bext

character(len=255)    :: filename
integer               :: err
  
integer(hid_t)                 :: fid, gid, aid, did
integer(hsize_t), dimension(1) :: cm3 = (/3/), cm1 = (/1/)


write(filename,"(a,a1,i6.6,'_',i5.5,'.h5')") trim(dir_data), ftype, snap, 0

call h5open_f(err)

  call h5fopen_f(filename, h5f_acc_rdwr_f, fid, err)

    call h5gopen_f(fid, 'attributes', gid, err)
    
      call h5aopen_name_f(gid, 'bext', aid, err)
      call h5awrite_f(aid, h5t_native_real, bext, cm3, err)
      call h5aclose_f(aid, err)
    
    call h5gclose_f(gid, err)

  call h5fclose_f(fid, err)

call h5close_f(err)


end subroutine hdf5_overwrite_bext
!##############################################################################

end module hdf5_io
