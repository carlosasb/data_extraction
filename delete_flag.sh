#!/bin/bash

if [ ! -d "$1" ]
then
    echo "##### Argument "$1" is NOT a directory! #####"
    exit 1
fi

fname=$(pwd)"/output.txt"
if [ -e "$fname" ]
then
    rm $fname
fi
find "$1" -type d >> $fname

for i in {15..15..5}
do
    ang=$i
    for vel in {10..10..5}
    do
        for k in {1..5..1}
	do
            smass=$((k*10))
            for sig in {100..100..100}
            do
                regex="ang$ang/vel$vel/smass$smass/sig$sig"
                pname=$(grep -h $regex $fname)
                flag=$pname"/flag"
                if [[ -n $pname ]]
                then
                    if [[ -e $flag ]]
                    then
                        rm -f $flag
                    fi
                fi
            done
        done
    done
done
