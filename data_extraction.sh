#!/bin/bash

# just testing branching

#=================================================================
# Checks to see if the argument passed is a directory.
# Argument should be the parent directory where the 
# godunov code outputs (*.h5) are stored.
#=================================================================
if [ ! -d "$1" ]
then
    echo "##### Argument "$1" is NOT a directory! #####" > error.txt
    exit 1
fi

#=================================================================

begindate=$(date)

#=================================================================
# Same check as the first argument. If not null, directory will be 
# created. In case the directory string is null, directory will
# be set as local directory.
# DPNAME => target parent directory for the extracted data from 
# the godunov code outputs *.h5.
#=================================================================
if [[ ! -n $2 ]]
then
    echo "" >> error.txt
    echo "Target parent directory "$2" does not exist" >> error.txt
    echo "Making directory" >> error.txt
    mkdir $2
    dpname=$2"/"
else
    echo "" >> error.txt
    echo "Target directory "$2" is null" >> error.txt
    echo "Setting directory to local directory" >> error.txt
    dpname=$(pwd)"/"
fi

#=================================================================

tfile="default.txt"
while read line
do
    read -a allt <<< `./lineparser.x "$line" " "`
done < $tfile

#=================================================================
# Checks to see if the output.txt file already exists and deletes 
# it in case it does. Also checks for the sed* files created 
# during the gnuplot script execution, deleting it in case they 
# are found.
# FNAME => location of the script and where the data is extracted
# prior to its relocation in DPNAME. Concatenated with the 
# output.txt file name.
# GARBAGE => location of the sed* garbage files generated during
# the gnuplot script runtime.
#=================================================================
fname=$(pwd)"/output.txt"
garbage=$(pwd)"/sed*"
if [ -e "$fname" ]
then
    rm $fname
fi
if [ -e "$garbage" ]
then
    rm -f $garbage
fi
#=================================================================

#=================================================================
# Uses the find command to obtain the directory tree for the 
# $1 primary directory. Prints results to the output.txt file 
# located in FNAME.
# will add new stuff
#=================================================================
find "$1" -type d >> $fname 

#=================================================================
# Definition of the types of data and plots used for analysis.
# YRES    => scalar integer corresponding to the y resolution
# GRID    => scalar integer corresponding to a forth the y range
# NFIELD  => 7 element array storing the types of data to be 
# extracted via the plot_field tool. Se the code for plot_field
# for information of the types of data.
# FIGTIPE => 7 element array storing the final figure names
# obtained the gnuplot plotting tool. These represent the first 
# image to be created and are iterated from 1 to 151.
# PTYPE   => 7 element array storing the type of plot used by 
# gnuplot. See gnuplot help for the plot command for details.
# FORTYPE => 7 element array storing the plotting format used by 
# gnuplot. See gnuplot help for the plot command for details.
# COM     => 7 element array storing range in y used in each image.
# All ranges in x are the same.
# FIGMAX  => 4 element array storing the figure name for the 
# maximum value plot. See plot_fields for details.
# COMMAX  => 4 element array analogue to COM, only for the maximum
# value plots. 
#=================================================================
yres=1024
grid=$((yres/4))
nfield[0]='file_type'
nfield[1]='dens'
nfield[2]='denc'
nfield[3]='velc'
nfield[4]='vell'
nfield[5]='velr'
nfield[6]='velv'
nfield[7]='csnd'
cblabel=("cblabel \"labelcb\"" "cblabel \"n (cm^{-3})\"" "cblabel \"labelcb\""\ 
"cblabel \"Number density n (cm^{-3})\"" "cblabel \"Number density n (cm^{-3})\""\ 
"cblabel \"Number density n (cm^{-3})\"" "cblabel \"Number density n (cm^{-3})\"" "cblabel \"c_{s} (km s^{-1})\"")
cbrange=("cbrange\[\]" "cbrange\[10:5000\]" "cbrange\[10:2000\]" "cbrange\[10:2000\]" "cbrange\[10:2000\]" "cbrange\[10:2000\]" "cbrange\[10:2000\]"\ 
"cbrange\[1:1000\]")
ytics=("yformat_increment" "ytics (\"-0.5\" 1,\"-0.25\" $((grid*1)),\"0\" $((grid*2)),\"0.25\" $((grid*3)),\"0.50\" $((grid*4)))"\ 
"format y \"%1.2g\" \; set ytics 0.0\*rho 50.0\*rho"\ 
"format y \"%1.2g\" \; set ytics -0.1\*vel 0.5\*vel"\ 
"format y \"%1.2g\" \; set ytics 0.01\*vel 0.3\*vel"\ 
"format y \"%1.2g\" \; set ytics 0.01\*vel 0.3\*vel"\ 
"ytics (\"-0.5\" 1,\"-0.25\" $((grid*1)),\"0\" $((grid*2)),\"0.25\" $((grid*3)),\"0.50\" $((grid*4)))"\ 
"ytics (\"-0.5\" 1,\"-0.25\" $((grid*1)),\"0\" $((grid*2)),\"0.25\" $((grid*3)),\"0.50\" $((grid*4)))")
ylabel=("nomey" "Y (kpc)" "n (cm^{-3})" "V_{y} (km s^{-1})" "V_{x} (km s^{-1})" "V_{x} (km s^{-1})" "Y (kpc)" "Y (kpc)")
xlabel=("nomex" "X (kpc)" "Y (kpc)" "Y (kpc)" "X (kpc)" "Y (kpc)" "X (kpc)" "X (kpc)")
figtype=("figure_name.png" "${nfield[1]}0.png" "${nfield[2]}0.png" "${nfield[3]}0.png" "${nfield[4]}0.png" "${nfield[5]}0.png"\
"${nfield[6]}0.png" "${nfield[7]}0.png")
ptype=("plot_type" "image" "points" "points" "points" "points" "vec" "image")
fortype=("format_type" "u 1:2:(\$3\*rho)" "u 1:(\$2\*rho)" "u 1:(\$2\*vel)" "u 1:(\$2\*vel)" "u 1:(\$2\*vel)" "u 1:2:(\$3\*scale):(\$4\*scale)"\ 
"u 1:2:(\$3\*vel)")
com=("yrange\[\]" "yrange\[1:$yres\]" "yrange\[0.0\*rho:500.0\*rho\]" "yrange\[-1.0\*vel:5.0\*vel\]" "yrange\[0.1\*vel:3.0\*vel\]" "yrange\[0.1\*vel:3.0\*vel\]"\ 
"yrange\[1:$yres\]" "yrange\[1:$yres\]")
#==================================================================

echo ${ytics[0]} ${ytics[4]} ${nfield[4]} > echo_error.txt 2>&1

#==================================================================
# Initialization for the field name, path and iteration for 
# use in plot_fields. 
# NFOLD    => initial value of the NFIELD variable in the 
# parameters.in input file for plot_fields.
# PNAMEOLD => initial value of the PNAME variable. See its 
# description for more details.
# AOLD     => initial value of the iteration A for each run of the 
# plot3.gnu gnuplot script. See its description for more details.
# L        => iteration for the maximum value plots. Serves as  
# index for the COMMAX and FIGMAX arrays.
# JOLD     => last value of the j iteration variable. Indexes the 
# NFIELD array when obtaining the maximum value plots.
#==================================================================
nfold='fname'
pnameold='input_path'
l=0
jold=0
timeold="time=1"
#==================================================================

#==================================================================
# Main block. The resolution is set in the plot3.gnu 
# script. The for loops iterate for the pitch angle, 
# the initial velocity and the mass of the gravitational 
# source. 
# XRES    => resolution in x axis. Default is same as y axis
# XRANGE  => global x axis range
# SPACING => jumpv value in parameters.in
# OUTPATH => dir_out value in parameters.in
#==================================================================
xres=1024
xrange="xrange\[1:$xres\]"
grid=$((xres/4))
xtics="xtics (\"-0.5\" 1,\"-0.25\" $((grid*1)),\"0\" $((grid*2)),\"0.25\" $((grid*3)),\"0.50\" $((grid*4)))"
spacing=$((xres/16))
outpath="${HOME}/hdf5_data_extractor/resultados_temp/"
sed -i "s/spacing/$spacing/g" 'parameters.in'
sed -i "s/xformat_increment/$xtics/g" 'plot3.gnu'
sed -i "s/xrange\[\]/$xrange/g" 'plot3.gnu'
sed -i "s|output_path|$outpath|g" 'parameters.in'

echo "########################################################" > log.txt
echo "##        Log for the data extraction tool            ##" >> log.txt
echo "## Run started at $begindate        ##" >> log.txt
echo "########################################################" >> log.txt

echo "Plot_fields" > log_plot_fields.txt
echo "Plot3" > log_gnuplot.txt

#for gamma in {11..12..1}
#do
    for i in {15..15..5}
    do
        # ANG => value of the pitch angle, in degrees
        ang=$i
        for vel in {10..10..5}
        do
            for k in {10..50..10}
            do
                # SMASS => value of the source mass in code units 
                smass=$k
                for sig in {100..100..100}
                do
                    # REGEX => the regex string used for choosing the 
                    regex="ang$ang/vel$vel/smass$smass/sig$sig"
                    pname=$(grep -h $regex $fname)
                    flag=$pname"/flag"
                    if [ -n "$pname" ]
                    then
                        pname=$pname"/"
                        sed -i "s|$pnameold|$pname|g" 'parameters.in'
                        if [ -e "$flag" ]
                        then
                           echo "" >> log.txt
                           echo "Flag found in directory $pname. Moving to the next directory." >> log.txt
                        else
                            firstout=$pname"p000000_00000.h5"
                            if [ -e "$firstout" ]
                            then
                                echo "" >> log.txt
                                echo "Output file found in directory $pname." >> log.txt
                                current=$(date)
                                echo "Began at $current" >> log.txt
                                for j in {1..7..1} 
                                do
                                    sed -i "s/$nfold/${nfield[j]}/g" 'parameters.in'
                                    nfold=${nfield[j]}
                                    sed -i "s/${nfield[0]}/${nfield[j]}/g" 'plot3.gnu'
                                    sed -i "s/${ptype[0]}/${ptype[j]}/g" 'plot3.gnu'
                                    sed -i "s/${fortype[0]}/${fortype[j]}/g" 'plot3.gnu'
                                    sed -i "s/${com[0]}/${com[j]}/g" 'plot3.gnu'
                                    sed -i "s/${xlabel[0]}/${xlabel[j]}/g" 'plot3.gnu'
                                    sed -i "s/${ylabel[0]}/${ylabel[j]}/g" 'plot3.gnu'
                                    sed -i "s/${ytics[0]}/${ytics[j]}/g" 'plot3.gnu'
                                    sed -i "s/${cblabel[0]}/${cblabel[j]}/g" 'plot3.gnu'
                                    sed -i "s/${cbrange[0]}/${cbrange[j]}/g" 'plot3.gnu'
                                    figold=${figtype[0]}
                                    aold="snap_first = 0"
                                    bold="snap_last  = 0"
                                    echo "===================================================" >> log_plot_fields.txt 2>&1
                                    echo ${nfield[j]} >> log_plot_fields.txt 2>&1
                                    echo "===================================================" >> log_plot_fields.txt 2>&1
                                    echo "===================================================" >> log_gnuplot.txt 2>&1
                                    echo ${nfield[j]} >> log_gnuplot.txt 2>&1
                                    echo "===================================================" >> log_gnuplot.txt 2>&1
                                    wt=0
                                    echo ${ytics[0]} ${ytics[j]} ${nfield[j]} >> echo_error.txt 2>&1
                                    for t in {0..150..1}
                                    do
                                        anew="snap_first = $t"
                                        bnew="snap_last  = $t"
                                        sed -i "s/$aold/$anew/g" 'parameters.in'
                                        sed -i "s/$bold/$bnew/g" 'parameters.in'
                                        ${HOME}/data_extraction/plot_fields/plot_fields.x  >> log_plot_fields.txt 2>&1
                                        if [ "$j" -eq 6 ]
                                        then
                                            sed -i "s/$nfold/dens/g" 'parameters.in'
                                            ${HOME}/data_extraction/plot_fields/plot_fields.x  >> log_plot_fields.txt 2>&1
                                            sed -i "s/dens/$nfold/g" 'parameters.in'
                                        fi
                                        fignew=${nfield[j]}$t".png"
                                        timenew="time="$t
                                        sed -i "s/$figold/$fignew/g" 'plot3.gnu'
                                        sed -i "s/$timeold/$timenew/g" 'plot3.gnu'
                                        sed -i "s/$figold/$fignew/g" 'plot2.gnu'
                                        sed -i "s/$timeold/$timenew/g" 'plot2.gnu'
#                                        if [ "$j" -eq 6 ]
#                                        then
#                                            gnuplot plot2.gnu   >> log_gnuplot.txt 2>&1
#                                        else
#                                            gnuplot plot3.gnu   >> log_gnuplot.txt 2>&1
	#                                        fi
                                        figold=$fignew
                                        timeold=$timenew
                                        aold=$anew
                                        bold=$bnew
                                        outt=${allt[wt]}
                                        intt=$((outt/20))
                                        if (( $t == $intt ))
                                        then
                                            mv "${nfield[j]}_slices_xy.dat" "${nfield[j]}_slices_xy_time${intt}_vel${vel}_smass${smass}.dat" >> error.txt 2>&1
                                            wt=$((wt+1))
                                        else
                                            rm ${nfield[j]}_slices_xy.dat >> error.txt 2>&1
                                        fi
                                    done
                                    sed -i "s/$aold/snap_first = 0/g" 'parameters.in'
                                    sed -i "s/$bold/snap_last  = 0/g" 'parameters.in'
                                    sed -i "s/$figold/${figtype[0]}/g" 'plot3.gnu'
                                    sed -i "s/$timeold/time=1/g" 'plot3.gnu'
                                    sed -i "s/$figold/${figtype[0]}/g" 'plot2.gnu'
                                    sed -i "s/$timeold/time=1/g" 'plot2.gnu'
                                    sed -i "s/${ylabel[j]}/nomey/g" 'plot3.gnu'
                                    sed -i "s/${xlabel[j]}/nomex/g" 'plot3.gnu'
                                    sed -i "s/${figtype[j]}/figure_name.png/g" 'plot3.gnu'
                                    sed -i "s/${nfield[j]}/file_type/g" 'plot3.gnu'
                                    sed -i "s/${ptype[j]}/plot_type/g" 'plot3.gnu'
                                    sed -i "s/${fortype[j]}/format_type/g" 'plot3.gnu'
                                    sed -i "s/${com[j]}/yrange\[\]/g" 'plot3.gnu'
                                    sed -i "s/${ytics[j]}/${ytics[0]}/g" 'plot3.gnu'
                                    sed -i "s/${cblabel[j]}/${cblabel[0]}/g" 'plot3.gnu'
                                    sed -i "s/${cbrange[j]}/${cbrange[0]}/g" 'plot3.gnu'
                                done
                                rpname="$dpname"$regex
                                if [ ! -d "$rpname" ]
                                then
                                   mkdir -p "$rpname"
                                fi
                                cp *.png "$rpname" >> error.txt 2>&1
                                rm *.png >> error.txt 2>&1
                                echo -e "" >> $flag
                            else 
                                echo  "" >> log.txt
                                echo "Warning!!! No output file found in directory $pname!" >> log.txt
                                echo "Check to see if simulation was successfully run in $pname." >> log.txt
                            fi
                        fi
                        pnameold=$pname
                    else
                        echo "$pname is not a valid path!" >> error.txt
                    fi
                done
            done
        done
    done
#done
#==================================================================
pnamei='input_path'
nfieldi='fname'
sed -i "s|$pnameold|$pnamei|g" 'parameters.in'
sed -i "s/$nfold/$nfieldi/g" 'parameters.in'
sed -i "s/$xrange/xrange\[\]/g" 'plot3.gnu'
sed -i "s/$xtics/xformat_increment/g" 'plot3.gnu'
sed -i "s/$spacing/spacing/g" 'parameters.in'
sed -i "s|$outpath|output\_path|g" 'parameters.in'
datloc="${dpname}/datfiles/"
if [ ! -d "$datloc" ]
then
    mkdir "$datloc"
fi
mv *.dat "$datloc"
rm $fname
rm -f $garbage

enddate=$(date)

echo "" >> log.txt
echo "########################################################" >> log.txt
echo "## Run finished at $enddate       ##" >> log.txt
echo "########################################################" >> log.txt
