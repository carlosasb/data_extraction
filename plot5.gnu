#Visualização de MDT com Gnuplot
# Autor: M. Galo, UNESP / Departamento de Cartografia
# 
#
#reset
#set term postscript enhanced color 
set term png enhanced
set output "figname.png"
set xlabel "Integration time t (Gyr)" offset character 0,0.5,0
set ylabel "{\/Symbol r}_{max} (g cm^{-3})" offset character 2.3,0,0
set xtics font "{},8"
set ytics font "{},8"
utime=0.09786
udens=1.92E-25
x=0*0.2*utime
y1=0*udens
y2=30*udens
set xrange[0:3]
set yrange[y1:y2]
set size 1 
set style line 1 lt 1 lw 3 ps 10
set arrow from x,y1 to x,y2 nohead 
plot 'dens_max_xy.dat' u ($1*utime):($2*udens) w line notitle lc rgb 'blue' ,\
'dens_max_xy.dat' dtype u ($1*utime):($2*udens) w point notitle lc rgb 'red'
