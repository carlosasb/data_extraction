#Visualização de MDT com Gnuplot
# Autor: M. Galo, UNESP / Departamento de Cartografia
# 
#
#reset
set term png enhanced font "/sto/home/cbraga/fonts/TTF/arial.ttf" 
set output "figure_name.png"
set cblabel "n (cm^{-3})"   offset character 0,-0.7,0
set xlabel "X (kpc)" offset character 0,0.5,0
set ylabel "Y (kpc)" offset character 2.3,0,0
tc=0.2*0.09786
time=18*tc
set title sprintf("Integration time = %3.4f Gyr",time) offset character 0,-0.6,0
rho=1.92E-23/(1.6733E-24*1.0E0)
vel=10.0
set palette model RGB
set palette defined ( 0 "light-cyan", 2 "cyan", 4 "blue", 8 "dark-blue", 10 "black")
set xrange[1:1024]
set yrange[1:1024]
set cbrange[10:5000] 
set xtics ("-0.5" 1,"-0.25" 256,"0" 512,"0.25" 768,"0.50" 1024)
set xtics font "/sto/home/cbraga/fonts/TTF/arial.ttf, 8"
set ytics ("-0.5" 1,"-0.25" 256,"0" 512,"0.25" 768,"0.50" 1024) 
set ytics font "/sto/home/cbraga/fonts/TTF/arial.ttf, 8"
#set logscale cb
set format cb "%1.2g" ; set cbtics font "/sto/home/cbraga/fonts/TTF/arial.ttf, 8"
set size 1
scale=50
plot 'dens_slices_xy.dat' u 1:2:($3*rho) w image notitle,\
'velv_slices_xy.dat' u 1:2:($3*scale):($4*scale) w vec notitle lc rgb "red"
