#Visualização de MDT com Gnuplot
# Autor: M. Galo, UNESP / Departamento de Cartografia
# 
#
#reset
set term png enhanced font "/sto/home/cbraga/fonts/TTF/arial.ttf" 
set output "figure_name.png"
set cblabel "labelcb"    offset character 0,0.3,0
set xlabel "nomex" offset character 0,0.5,0
set ylabel "nomey" offset character 2.3,0,0
tc=0.2*0.09786
time=18*tc
set title sprintf("Integration time = %3.4f Gyr",time) offset character 0,-0.6,0
rho=1.92E-23/(1.6733E-24*1.0E0)
vel=10.0
set palette model RGB
set palette defined ( 0 "light-cyan", 2 "cyan", 4 "blue", 8 "dark-blue", 10 "black")
set xrange[]
set yrange[]
set cbrange[] 
set xformat_increment
set xtics font "/sto/home/cbraga/fonts/TTF/arial.ttf, 8"
set yformat_increment 
set ytics font "/sto/home/cbraga/fonts/TTF/arial.ttf, 8"
#set logscale cb
set format cb "%1.2g" ; set cbtics font "/sto/home/cbraga/fonts/TTF/arial.ttf, 8"
set size 1
scale=50
plot 'file_type_slices_xy.dat' format_type w plot_type notitle lc 3 pt 7 ps 0.3
